package com.geektrust.traffic.model;

import com.geektrust.traffic.constant.Orbit;
import com.geektrust.traffic.constant.Vehicle;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Output {
    Vehicle vehicle;
    Orbit orbit;

    @Override
    public String toString() {
        return vehicle + " " + orbit;
    }
}
