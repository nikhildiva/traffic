package com.geektrust.traffic.model;

import com.geektrust.traffic.constant.Orbit;
import com.geektrust.traffic.constant.Weather;
import lombok.Value;

import java.util.HashMap;
import java.util.Map;

@Value
public class Input {
    Weather weather;
    Map<Orbit, Integer> speeds;

    public   Input(String inputLine) {
        String[] splitString = inputLine.split(" ");

        weather = Enum.valueOf(Weather.class, splitString[0]);
        speeds = new HashMap<>();

        int speedIndex = 0;
        for (Orbit orbit : Orbit.values()) {
            speeds.put(orbit, Integer.parseInt(splitString[++speedIndex]));
        }
    }

    public int getOrbitSpeed(Orbit orbit) {
        return speeds.get(orbit);
    }
}
