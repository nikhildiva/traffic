package com.geektrust.traffic.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum Orbit {
    ORBIT1(18, 20),
    ORBIT2(20, 10);

    private final int distance;
    private final int craterCount;
}
