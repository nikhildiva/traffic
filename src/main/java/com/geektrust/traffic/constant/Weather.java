package com.geektrust.traffic.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum Weather {

    SUNNY(0.9F),
    RAINY(1.2F),
    WINDY(1);

    private final float craterFormFactor;
}
