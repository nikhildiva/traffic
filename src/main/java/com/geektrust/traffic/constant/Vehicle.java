package com.geektrust.traffic.constant;

import lombok.Getter;

import java.util.stream.Stream;

@Getter
public enum Vehicle {

    BIKE(10,
            2,
            1,
            new Weather[]{Weather.SUNNY, Weather.WINDY}),
    TUKTUK(12,
            1,
            2,
            new Weather[]{Weather.SUNNY, Weather.RAINY}),
    CAR(20,
            3,
            3,
            new Weather[]{Weather.SUNNY, Weather.RAINY, Weather.WINDY});

    private final int speedInMmPHr;
    private final int craterTimeInMinutes;
    // Keeping preference as a variable because we can't restrict the order while adding more enum objects
    private final int preference;
    private final Weather[] suitableWeathers;

    Vehicle(int speedInMmPHr, int craterTimeInMinutes, int preference, Weather[] suitableWeathers) {
        this.speedInMmPHr = speedInMmPHr;
        this.craterTimeInMinutes = craterTimeInMinutes;
        this.preference = preference;
        this.suitableWeathers = suitableWeathers;
    }

    public boolean canRideInWeather(Weather weather) {
        return Stream.of(suitableWeathers)
                .anyMatch(thisWeather -> thisWeather == weather);
    }
}
