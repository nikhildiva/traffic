package com.geektrust.traffic.support;

import com.geektrust.traffic.constant.Orbit;
import com.geektrust.traffic.constant.Vehicle;
import com.geektrust.traffic.constant.Weather;
import com.geektrust.traffic.model.Input;
import com.geektrust.traffic.model.Output;

public class SpeedCalculationSupport {

    private SpeedCalculationSupport() {
    }

    public static float getEffectiveTravelTime(Input input, Vehicle vehicle, Orbit orbit) {

        Weather weather = input.getWeather();
        if (!vehicle.canRideInWeather(weather)) {
            return Float.MAX_VALUE;
        }

        int speed = Integer.min(input.getOrbitSpeed(orbit), vehicle.getSpeedInMmPHr());
        float craters = weather.getCraterFormFactor() * orbit.getCraterCount();
        float totalCraterTimeInHours = (craters * vehicle.getCraterTimeInMinutes()) / 60;
        float totalTravelTimeInHours = (float) orbit.getDistance() / speed;

        return totalCraterTimeInHours + totalTravelTimeInHours;
    }

    public static Output getEfficientTravelMean(Input input) {

        Output output = new Output(null, null);
        float currentMinTime = Float.POSITIVE_INFINITY;
        for (Vehicle vehicle : Vehicle.values()) {
            for (Orbit orbit : Orbit.values()) {
                float travelTime = getEffectiveTravelTime(input, vehicle, orbit);
                if (travelTime < currentMinTime) {
                    currentMinTime = travelTime;
                    output = new Output(vehicle, orbit);
                } else if (travelTime == currentMinTime &&
                        output.getVehicle() != null &&
                        output.getVehicle().getPreference() > vehicle.getPreference()) {
                    output = new Output(vehicle, orbit);
                }
            }
        }

        return output;
    }


}
