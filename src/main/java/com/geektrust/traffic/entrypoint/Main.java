package com.geektrust.traffic.entrypoint;

import com.geektrust.traffic.model.Input;
import com.geektrust.traffic.model.Output;
import com.geektrust.traffic.support.SpeedCalculationSupport;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;

/**
 * Assumption:
 * 1) Input will have as many orbit speeds as orbits. i.e. 2 currently
 */
public class Main {
    public static void main(String[] args) {
        if (args.length == 0) {
            System.exit(1);
        }

        Path path = Paths.get(args[0]);

        try (Stream<String> stream = Files.lines(path)) {
            stream.forEach(
                    line -> {
                        Input input = new Input(line);
                        Output output = SpeedCalculationSupport.getEfficientTravelMean(input);
                        System.out.println(output);
                    }
            );
        } catch (IOException e) {
            //handle bad file
        }
    }
}